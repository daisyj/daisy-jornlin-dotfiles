#!/usr/bin/env python3

import re

try:
    with open('/sys/class/power_supply/hidpp_battery_1/uevent', 'r') as content_file:
        in_file = content_file.read()
except:
    in_file = 'POWER_SUPPLY_MODEL_NAME=err\
POWER_SUPPLY_CAPACITY=0'

device_name = re.findall(r'POWER_SUPPLY_MODEL_NAME=((?:(?!\n).)*)', in_file, re.DOTALL)
battery = re.findall(r'POWER_SUPPLY_CAPACITY=((?:(?!\n).)*)', in_file, re.DOTALL)

if device_name[0] == "M570":
    print(" " + battery[0] + "%")
elif device_name[0] == "Wireless Solar Keyboard K750":
    print(" " + battery[0] + "%")
else:
    print("")

# print("Mouse Battery Goes Here!")
