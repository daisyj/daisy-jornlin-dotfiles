#!/usr/bin/env bash

# Terminate already running bar instances
polybar-msg cmd quit

echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log /tmp/polybar3.log

rm /tmp/ipc-pixiebar
rm /tmp/ipc-starbar
rm /tmp/ipc-pixietray
rm /tmp/ipc-startray
echo 'ipc symlinks deleted'

polybar --config=$HOME/.config/polybar/config-dark -r pixiebar >> /tmp/polybar1.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-pixiebar

polybar --config=$HOME/.config/polybar/config-dark -r starbar >> /tmp/polybar2.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-starbar

polybar --config=$HOME/.config/polybar/config-dark -r pixietray >> /tmp/polybar3.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-pixietray

polybar --config=$HOME/.config/polybar/config-dark -r startray >> /tmp/polybar4.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-startray

echo "Bars launched..."
