#!/usr/bin/env bash

# Terminate already running bar instances
polybar-msg cmd quit

echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log /tmp/polybar3.log


polybar --config=$HOME/.config/polybar/config-light -r pixiebar >> /tmp/polybar1.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-pixiebar

polybar --config=$HOME/.config/polybar/config-light -r starbar >> /tmp/polybar2.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-starbar

polybar --config=$HOME/.config/polybar/config-light -r pixietray >> /tmp/polybar3.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-pixietray

polybar --config=$HOME/.config/polybar/config-light -r startray >> /tmp/polybar4.log 2>&1 &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-startray

echo "Bars launched..."
