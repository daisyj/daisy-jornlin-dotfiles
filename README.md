# Daisy Jornlin Dotfiles

These are the current dotfiles for Daisy Jornlin's desktop.

There are currently configuration files in here for:
-   i3
    -   i3wm
    -   i3status
    -   py3status
-   Picom
-   Polybar
    -   Scripts for Polybar for displaying the battery level for connected peripherals
        -   These scripts are currently configured for:
            -   a single Logitech M570 wireless trackball
		    -   a single Logitech K750 wireless solar powered keyboard
-   Rofi
